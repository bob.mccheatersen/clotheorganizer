This a school project about organizing clothe.

To use the project:

1. Connect an arduino with an Adafruit Industries PN532 NFC/RFID reader/writer
shield to the computer.

2. Download and install the Adafruit-PN532 library:
https://github.com/adafruit/Adafruit-PN532

3. Compile the ArduinoRFIDScript found in the similiar named folder and upload.
it to the arduino.

4. Close anything that could be using the port that the arduino is connected to.

5. Run the java project by double clicking the "server/ClotheOrganizer.jar".
This assumes that java 8 or later is installed on the pc.

6. Set up a flutter environment according to this video: 
https://youtu.be/GLSG_Wh_YWc?t=458

7. Open the app sourcecode (open "app/test_app") and set the gemtSocket 
constructor string to the ip of the machine the java project is running on
(line 119). 

8. In VS Code go to pubspec.yaml and press the "Get Packages" arrow in the top
right corner.

9. Under "debug" in the top left menu press "start without debugging", and 
choose any emulator. (see step 6.) 

8. Go to app/test_app/build/app/outputs/apk and export the app.apk to an android
device (through USB, mail or other methods etc.).

9. Run the app.