package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestClient {

	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {


		try(
				Socket s = new Socket("127.0.0.1", 8877);
				OutputStream out = s.getOutputStream();
				InputStream in = s.getInputStream();
				BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
				){

			while(true) {
				String command = stdIn.readLine();
				
				System.out.println("sending: " + command);
				
				out.write(command.getBytes());
				
				byte[] bytes = new byte[2048];
				
				in.read(bytes);
				
				System.out.println(new String(bytes));
				
			}
			


		}


	}

}
