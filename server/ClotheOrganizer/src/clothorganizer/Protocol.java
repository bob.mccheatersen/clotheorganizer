package clothorganizer;

/**
 * The Protocol interface describes a text based protocol, that takes an input and returns a response.
 * It contains three static, final, pre-implemented Protocols
 * 
 * @author Valdemar Friis
 * @since 2019-04-29
 */
@FunctionalInterface
public interface Protocol {
	
	/**
	 * Test protocol that simply sends back its input
	 */
	public static final Protocol ECHO = (s) -> {
		return s;
	};
	
	/**
	 * This protocol is used for data requests from the client.
	 * Currently, the only type of request is the time data for each cloth.
	 * 
	 * if it successfully recognizes what data is being requested, it will return that data as a string
	 * otherwise it will return "unknown_request"
	 */
	public static final Protocol REQUEST = (s) -> {
		if(s.equals("request:cloth_time_data")) {
			Cloth[] cloths = FileHandler.getInstance().getClothes();
			String response = "";
			
			for(int i = 0; i < cloths.length; i++) {
				response += cloths[i].toString() + "\n";
			}
			
			return response;
		}
		
		return "unknown_request";
	};

	/**
	 * This protocol is used to rename clothing.
	 * It needs the id and new name separated by a tab.
	 * If the id is unknown, it will return "rename_unknown_id"
	 * if it found the clothing with the specified id and renamed it, it will return "rename_success"
	 */
	public static final Protocol RENAME = (s) -> {
		
		s = s.replaceFirst("rename:", "");
		
		try {
			FileHandler.getInstance().rename(Integer.parseInt(s.substring(0, s.indexOf("\t"))), s.substring(s.indexOf("\t") + 1, s.length()));
		}catch(NumberFormatException | UnknownIdException e) {
			e.printStackTrace();
			return "rename_unknown_id";
		}
		
		return "rename_success";
	};
	
	/**
	 * This protocol instantly disconnects an app by returning null.
	 */
	public static Protocol DISCONNECT = (s) -> {
		return null;
	};
	
	/**
	 * This protocol finds out which of the other protocols to use.
	 * It does so by checking what the command starts with.
	 * It will return whatever the protocol it found says, or "unknown_command" if no protocol was found.
	 */
	public static final Protocol DEFAULT = (s) -> {
		if(s.startsWith("request:")) {
			return REQUEST.getResponse(s);
		}else if(s.startsWith("rename:")){
			return RENAME.getResponse(s);
		}else if(s.startsWith("disconnect:")) {
			return DISCONNECT.getResponse(s);
		}
		
		return "unknown_command";
	};
	
	public String getResponse(String input);
	
}
