package clothorganizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
/**
 * The FileHandler class is responsible for saving the data the RFIDHandler collects and sending it through to AppHandlers.
 * It is based of a singleton pattern.
 * 
 * @author Valdemar Friis
 * @since 2019-04-27
 */
public final class FileHandler {
	
	private static FileHandler instance;
	
	static {
		try {
			instance = new FileHandler("C:\\clothData\\data.txt");
		} catch (FailedToInitializeException e) {
			e.printStackTrace();
		}
	}
	
	private File dataFile;
	private List<Cloth> cloths = new ArrayList<>();
	
	private FileHandler(String dataFilePath) throws FailedToInitializeException {
		dataFile = new File(dataFilePath);
		
		if(!dataFile.exists()) {
			try {
				dataFile.createNewFile();
			} catch (IOException e) {
				throw new FailedToInitializeException("Could not create fileHandler instance", e);
			}
		}
		
		try(
				// create BufferedReader to read from file
				BufferedReader in = new BufferedReader(new FileReader(dataFile));
				){
			
			String line = null;
			
			// reads all lines from data file 
			while((line = in.readLine()) != null) {
				cloths.add(new Cloth(line));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * The register method allows other objects to register new clothes.
	 * 
	 * @param c the cloth thats being registered
	 */
	private void register(Cloth c) {
		// add cloth to cloth list
		cloths.add(c);
		rewriteFile();
	}
	
	/**
	 * Wipes the data file and fills it with the cloth currently stored in clothes
	 */
	private void rewriteFile() {

		try(
				// open writer to file, without appending
				PrintWriter out = new PrintWriter(new FileWriter(dataFile, false));
				){
			
			// write all cloths to file
			for(int i = 0; i < cloths.size(); i++) {
				out.println(cloths.get(i));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Renames a piece of clothing.
	 * If the provided id is not on the current clothes List it will add it.
	 * 
	 * @param id the id of the cloth that is being renamed
	 * @param newName the new name of the cloth
	 * @throws UnknownIdException if the id is not on the clothes list
	 */
	public void rename(int id, String newName) throws UnknownIdException {
		
		boolean unknownId = true;
		
		for(int i = 0; i < cloths.size(); i++) {
			if(cloths.get(i).getId() == id) {
				cloths.get(i).setName(newName);
				unknownId = false;
			}
		}
		
		if(unknownId) {
			throw new UnknownIdException();
		}else {
			rewriteFile();
		}
		
	}
	
	/**
	 * Updates the time a piece of clothing on the list was last worn.
	 * If the id is not on the cloth list, a new cloth is added.
	 * 
	 * @param id the id of the cloth that is being update
	 */
	public void update(int id){
		
		boolean newcloth = true;
		
		// loop through all cloths
		for(int i = 0; i < cloths.size(); i++) {
			// if the current cloth matches id with the provided id
			if(cloths.get(i).getId() == id) {
				newcloth = false;
				// update that cloth
				cloths.get(i).updateLastWorn();
			}
		}
		
		// if the id didn't match that of any cloth object on clothes
		if(newcloth) {
			// create new cloth object named "unnamed"
			Cloth c = new Cloth(id, "unnamed");
			// register that cloth
			FileHandler.getInstance().register(c);
			System.out.println("RFIDHandler: registered: " + c);
		}else {
			// register() rewrites the file, so this is only necessary if register() isn't called
			rewriteFile();
		}
	}
	
	/**
	 * The getClothes method gets all clothes currently registered in the FileHandler
	 * 
	 * @return an array with all the saved clothes
	 */
	public Cloth[] getClothes() {
		
		Cloth[] clothArray = new Cloth[cloths.size()];
		
		for(int i = 0; i < clothArray.length; i++) {
			clothArray[i] = cloths.get(i);
		}
		
		return clothArray;
	}
	
	/**
	 * The getInstance methods gets the singleton instance of FileHandler.
	 * 
	 * @return The FileHandler instance of the program
	 */
	public static FileHandler getInstance() {
		return instance;
	}
	
}
