package clothorganizer;

/**
 * This exception is thrown if an app tries to modify a piece of clothing with an id that the server doesn't know
 * 
 * @author Valdemar Friis
 * @since 2019-05-01
 */
@SuppressWarnings("serial")
public class UnknownIdException extends Exception{
	
}
