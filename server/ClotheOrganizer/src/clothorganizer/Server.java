package clothorganizer;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * The server object, is responsible for awaiting app's making contact and starting a new AppHandler for each of them.
 * 
 * Just like RFIDHandler, it is a singleton and a thread.
 * 
 * @author Valdemar Friis
 * @since 2019-04-28
 */

public final class Server extends Thread implements Closeable {
	
	private static Server instance;
	
	static {
		try {
			instance = new Server(8877);
		} catch (FailedToInitializeException e) {
			e.printStackTrace();
		}
	}
	
	private ServerSocket socket;
	
	/**
	 * @param port the port to open the server on.
	 * @throws FailedToInitializeException if it was impossible to create the server.
	 */
	private Server(int port) throws FailedToInitializeException {
		
		try {
			// try creating server with specified port
			socket = new ServerSocket(port);
		} catch (IOException e) {
			// if failed, throw FailedToInitializeException
			throw new FailedToInitializeException("Could not create server", e);
		}
		
		// set thread name to ServerThread for the sake of debugging
		setName("ServerThread");
	}
	
	/**
	 * The run method listens for app's trying to connect to this program and creates new AppHandler's for each client.
	 * The run method is blocking, and supposed to be on its own thread.
	 */
	@Override
	public void run() {
		int i = 0;
		
		while(true) {
			
			try {
				// create new apphandler
				AppHandler ah = new AppHandler(socket.accept(), i++);
				ah.start();
			}catch (IOException e) {
				e.printStackTrace();
				break;
			}
			
		}
		
		
	}
	
	/**
	 * The close method closes the ServerSocket
	 * @throws IOException if the ServerSocket threw an IOException
	 * @see ServerSocket#close()
	 */
	@Override
	public void close() throws IOException {
		// if the socket exists and is not closed
		if(socket != null && !socket.isClosed()) {
			socket.close();
		}
	}
	
	/**
	 * The getInstance methods gets the singleton instance of Server.
	 * 
	 * @return The Server instance of the program
	 */
	public static Server getInstance() {
		return instance;
	}
}
