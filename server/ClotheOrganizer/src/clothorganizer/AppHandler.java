package clothorganizer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

/**
 * The AppHandler class is handling a TCP connection to an app. 
 * 
 * @author Valdemar Friis
 * @since 2019-04-29
 */
public class AppHandler extends Thread {

	private Socket socket;
	private InputStream in;
	private OutputStream out;

	/**
	 * @param s the socket connected to the app.
	 * @param appID the ID of the app
	 */
	public AppHandler(Socket s, int appID) {

		socket = s;

		try {
			in = s.getInputStream();
			out = s.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("App connected");
		// set thread name
		setName("AppThread" + appID);
	}

	/**
	 * The run method is an endless cycle of listening and responding to the client.
	 * It will use the DEFAULT protocol to respond.
	 * 
	 * This method is endlessly blocking and not supposed to be called by anything but the thread start.
	 * 
	 * @see Protocol#DEFAULT
	 */
	@Override
	public void run() {

		System.out.println("AppHandler started");

		try {

			while(!Main.shutdown) {

				// create empty byte array with 2 kB
				byte[] bytes = new byte[2048];
				// fill byte array with data from app
				in.read(bytes);

				// turn byte array into text
				String line = new String(bytes).trim();

				System.out.println(Thread.currentThread().getName() + ": received command: " + line);

				// get response according to protocol
				String response = Protocol.DEFAULT.getResponse(line);

				if(response == null || socket.isClosed()) {
					break;
				}

				bytes = response.getBytes();

				// respond
				synchronized(out) {
					out.write(bytes);
				}

			}

		} catch (IOException e) {

			e.printStackTrace();

		}finally {
			try {
				close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	/**
	 * Closes the socket connected to this AppHandlers app
	 * 
	 * @see Socket#close()
	 * @throws IOException if the socket throws an IOException on close
	 */
	public void close() throws IOException {
		if(socket != null && !socket.isClosed()) {
			socket.close();
		}
	}

	/**
	 * Gets all running AppHandlers.
	 * Running means that it's socket is not closed.
	 * 
	 * @return a Set with all currently active AppHandlers
	 */
	public static Set<AppHandler> getAllAppHandlers(){
		Set<Thread> threads = Thread.getAllStackTraces().keySet();

		Set<AppHandler> appHandlers = new HashSet<>();

		for(Thread t : threads) {
			if(t instanceof AppHandler && !((AppHandler) t).socket.isClosed()) {
				appHandlers.add((AppHandler) t);
			}
		}

		return appHandlers;
	}

	/**
	 * Send any string to a client connected to this AppHandler
	 * 
	 * @param msg the message that is to be sent
	 * @throws IOException if it was impossible to send the message (e.g. the socket is closed)
	 */
	public void send(String msg) throws IOException {

		byte[] bytes = msg.getBytes();

		synchronized (out) {
			out.write(bytes);
		}

	}

}
