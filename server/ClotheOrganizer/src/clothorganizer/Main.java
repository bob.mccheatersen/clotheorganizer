package clothorganizer;

/**
 * The Main class is just a main method holder.
 * 
 * @author Valdemar Friis
 * @since 2019-04-28
 */
public final class Main {
	
	public static boolean shutdown = false;
	
	private Main() {}
	
	/**
	 * @param args does nothing
	 */
	public static void main(String[] args) {
		
		if(Server.getInstance() == null || FileHandler.getInstance() == null || RFIDHandler.getInstance() == null) {
			return;
		}
		
		Server.getInstance().start();
		
	}
	
}
