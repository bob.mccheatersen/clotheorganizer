package clothorganizer;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import com.fazecast.jSerialComm.SerialPort;

/**
 * RFIDHandler controls the serial communication with the arduino.
 * 
 * The class is a singleton design, that is also a thread.
 * It is responsible for reading from an serial port on the computer, presumably an arduino with a RFID module 
 * 
 * @author Valdemar Friis
 * @since 2019-05-01
 */
public final class RFIDHandler extends Thread{

	private static RFIDHandler instance;

	static {
		try {
			instance = new RFIDHandler();
			instance.start();
		} catch (FailedToInitializeException e) {
			e.printStackTrace();
			instance = null;
		}

	}

	private RFIDHandler() throws FailedToInitializeException {
		// set thread name to "RFIDHandler"
		setName("RFIDHandler");
	}
	
	/**
	 * Tries to find a USB connected arduino.
	 * 
	 * If succeeded it will open a port to that arduino and wait for it to send an input.
	 * It will then parse the input as an integer and ask FileHandler to update.
	 * 
	 * If failed, it will call runWithRandom(), witch will just act the same as if there was an arduino,
	 * but with random numbers and time intervals.
	 */
	@Override
	public void run() {
		// get all connected serial ports
		SerialPort[] ports = SerialPort.getCommPorts();

		SerialPort arduinoPort = null;
		
		// loop through ports
		for(int i = 0; i < ports.length; i++) {
			// if the ports name contains "Arduino"
			if(ports[i].toString().contains("Arduino")) {
				// set arduinoPort to that port
				arduinoPort = ports[i];
			}
		}
		
		// if no such port was found, runWithRandom();
		if(arduinoPort == null) {
			runWithRandom();
			return;
		}
		
		Scanner arduinoReader = null;
		
		try {
			// if openPort() is unsuccessful
			if(!arduinoPort.openPort()) {
				System.out.println("could not open port to arduino");
				// stop this
				return;
			}else {
				System.out.println("successfully opened port to arduino");
			}
			
			// baud rate matches that in arduino script
			arduinoPort.setBaudRate(115200);
			// use semi-blocking with endless timeout
			arduinoPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
			
			// create scanner object on InputStream from the arduino's OutputStream
			arduinoReader = new Scanner(arduinoPort.getInputStream());
			
			// keep reading while that scanner has more data coming
			while(arduinoReader.hasNext()) {
				// get the next line from the scanner and parse it as int
				int id = Integer.parseInt(arduinoReader.nextLine());
				System.out.println("RFIDHandler: " + id + " detected");
				
				// update that id
				FileHandler.getInstance().update(id);
			}
			
			
		}finally {
			if(arduinoReader != null) {
				arduinoReader.close();
			}
		}
		
	}

	/**
	 * @see RFIDHandler#run()
	 */
	private void runWithRandom() {
		try {
			while(true) {
				// wait a random amount of time between 1 and 10 seconds
				Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 10000));
				
				// update a random id in interval [0, 19]
				FileHandler.getInstance().update(ThreadLocalRandom.current().nextInt(0, 20));
			}
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The getInstance methods gets the singleton instance of RFIDHandler.
	 * 
	 * @return The Server instance of the program
	 */
	public static RFIDHandler getInstance() {
		return instance;
	}

}
