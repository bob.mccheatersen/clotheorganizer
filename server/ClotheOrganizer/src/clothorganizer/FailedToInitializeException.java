package clothorganizer;

/**
 * This exception is thrown if it was impossible to create the singleton of one of the three singleton classes
 * 
 * @author Valdemar Friis
 * @since 2019-05-02
 */
@SuppressWarnings("serial")
public class FailedToInitializeException extends Exception {
	
	public FailedToInitializeException(String message, Throwable e) {
		super(message, e);
	}
	
}
