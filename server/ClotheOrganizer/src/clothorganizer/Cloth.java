package clothorganizer;

/**
 * cloth is a data structure class, representing a piece of clothing.
 * 
 * @author Valdemar Friis
 * @since 2019-04-27
 */
public class Cloth {
	
	public static final String cloth_REGEX = "[0-9]+\t[a-zA-Z0-9 ]+\t[0-9]+";
	
	private int id;
	private long lastWorn;
	private String name;
	
	/**
	 * This constructor creates a piece of cloth from a string containing its data.
	 * 
	 * @param s a String containing id\tname\tlastWorn
	 */
	public Cloth(String s) {
		id = Integer.parseInt(s.substring(0, s.indexOf("\t")).trim());
		
		s = s.substring(s.indexOf("\t") + 1, s.length());
		
		name = s.substring(0, s.indexOf("\t")).trim();
		
		s = s.substring(s.indexOf("\t") + 1, s.length());
		
		lastWorn = Long.parseLong(s);
	}
	
	/**
	 * Constructor for cloth that sets lastWorn to the time it's called and the id and name to provided values
	 * 
	 * @param id the desired id of cloth object
	 * @param name the desired name for the cloth object
	 */
	public Cloth(int id, String name) {
		this.id = id;
		this.name = name;
		lastWorn = System.currentTimeMillis();
	}
	
	/**
	 * sets lastWorn to now
	 */
	public void updateLastWorn() {
		lastWorn = System.currentTimeMillis();
	}
	
	/**
	 * renames this clothing to the provided string
	 * 
	 * @param newName the desired new name for this piece of clothing
	 */
	public void setName(String newName) {
		name = newName;
	}
	
	/**
	 * @return The internal ID of this cloth
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return The time this was last worn in UNIX time
	 */
	public long getLastWorn() {
		return lastWorn;
	}
	
	/**
	 * @return the user-given name for this piece of clothing
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * constructs a string from this objects fields that matches cloth_REGEX.
	 * this string format is the way of communicating clothing to the app.
	 * 
	 * @return a string with this objects fields
	 */
	@Override
	public String toString() {
		return id + "\t" + name + "\t" + lastWorn;
	}
	
}
