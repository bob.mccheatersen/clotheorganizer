import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'Billede.dart';
import 'package:duration/duration.dart';
import 'package:duration/locale.dart';
 
class Clothes
{
  final int id;
  String name;
  final String lastWorn; //todo lav til unix date

//constructer
  Clothes(this.id, this.name, this.lastWorn);
} 
 void main() {
   runApp(MyApp());
 }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tøjapp',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter teknologi test'),
    );
  }
}

class MyHomePage extends StatefulWidget { 
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  List<Clothes> linesTest = new List<Clothes>();

  void onData(List<int> rawdata)
  {
    linesTest.clear(); // clearer listen over Clothes 

    String data = new String.fromCharCodes(rawdata).trim(); // tager rawdata og laver det til tekst
    List<String> lines = data.split("\n");  // laver lang string data om til én string pr. cloth

    // loop over listen af clothes lines (som stadig er tekst)
    for (int i = 0; i < lines.length; ++i)
    {
      String line = lines[i]; // nuværende line 
      List<String> fields = line.split("\t"); //splitter clothes op efter tab ( en inddeling efter id, navn og tid)
      int id = int.parse(fields[0]); // lav id om til et tal fra tekst
      String name = fields[1]; // navnet på tøjet
      String lastWorn = fields[2]; // tid

      // laver tid om til et bedre format
      int millislastworn = new DateTime.now().millisecondsSinceEpoch - int.parse(lastWorn);  
      Duration tid = new Duration(milliseconds: millislastworn);

     
      Clothes clothes = new Clothes(id, name, printDuration(tid)); // opretter clothes objekt

      linesTest.add(clothes); // putter clothes ind i list     
    }
    gemtState.setState((){ gemtState._listmedtoej = linesTest; });
  }

  void requestdata() // spørg om data
  {
    gemtSocket.write('request:clothe_time_data'); // send anmodning om data todo to første skal være længden af string?
  }

  void setuplisten(Socket socket)
  {
    gemtSocket = socket; // gem socket
    gemtSocket.listen(onData); // lyt efter data og giv socket onData

    requestdata(); // spørg efter data første gang
  }

  void updatecloth(int id,String navn)
  {
    gemtSocket.write('rename:' + id.toString() + "\t" + navn);
  }


  void disconnect()
  {
    gemtSocket.write('disconnect:iamdead');
    gemtSocket.close();
  }

  Socket gemtSocket; //gem socket så den kan bruges i requestdata

  _MyHomePageState gemtState;

  @override
  _MyHomePageState createState() {
    gemtState = _MyHomePageState(linesTest);
    Socket.connect('172.20.10.3', 8877).then(setuplisten);
    return gemtState;
  }
}

class _MyHomePageState extends State<MyHomePage> {
  List<Clothes> _listmedtoej = new List<Clothes>();

  _MyHomePageState(List<Clothes> linesTest)
  {
    _listmedtoej = linesTest;
  }
  
  @override
  void dispose() {
    widget.disconnect();
    super.dispose();
  }

  // opdater cloth nr. idx med nyt navn, refresh UI og send til server
  void TextInputSubmitted(String navn, int idx)
  {
    _listmedtoej[idx].name = navn;
    setState(() {});
    widget.updatecloth(_listmedtoej[idx].id, navn);
  }

  // når man klikker på navn af tøj, så åbner dialog
  void ClothClicked(BuildContext context, int idx)
  {
    //showDialog tager en builderfunktion der laver en dialogboks
    // content er textfield som tager userinput, der kalder en funktion der ændrer navnet til userinput og sender data videre til server
    // navigator hopper ud af dialogboksen når den er done
    showDialog<bool>(
      context:context,
      builder:(_) => new AlertDialog(
        title: Text('Rename your clothing'),
        content:TextField(onSubmitted:(s)
        {
          TextInputSubmitted(s,idx);
          Navigator.of(context).pop(true);
        })
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body:GridView.count(
          // Create a grid with 2 columns. If you change the scrollDirection to
          // horizontal, this would produce 2 rows.
          crossAxisCount: 2,
          childAspectRatio: (2/3),
          children: List.generate(_listmedtoej.length, (index) {
            return Column(
              children: [
                FlatButton(
                  onPressed:(){ClothClicked(context,index);},
                  child:Text('\r\n${_listmedtoej[index].name}\r\nLast Worn: ${_listmedtoej[index].lastWorn} ago',
                    style: Theme.of(context).textTheme.subtitle,
                  )
                ),
                PickImage(_listmedtoej[index].id)
              ], 
            );
          }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var encoder = new Utf8Encoder();
           widget.requestdata();
           //widget.onData(encoder.convert("132\that\t${DateTime.now().millisecondsSinceEpoch-1000000}\n123\that2\t${DateTime.now().millisecondsSinceEpoch-10000}\n2222\that4\t${DateTime.now().millisecondsSinceEpoch-100000000000}\n20\that3\t${DateTime.now().millisecondsSinceEpoch-10000000}"));

          setState(() {
            _listmedtoej = widget.linesTest;
          });
        },
        tooltip: 'refresh',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
