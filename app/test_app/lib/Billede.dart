import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
 
class PickImage extends StatefulWidget {
  PickImage(this.id) : super();
  final int id;

  final String title = "Flutter Pick Image demo";
 
  @override
  _PickImageState createState() => _PickImageState();
}
 
class _PickImageState extends State<PickImage> {
  Future<File> imageFile;

  _PickImageState()
  {
    getApplicationDocumentsDirectory().then((d)
      {
        if (File('${d.path}/${widget.id}.jpg').existsSync())
        {
          setState((){imageFile = new Future<File>(() => File('${d.path}/${widget.id}.jpg'));});
        }
      });
      
  }
 
  void pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
      getApplicationDocumentsDirectory().then((d)
      {
        imageFile.then((i)
        {
           i.copy('${d.path}/${widget.id}.jpg');
        });
      });
      
    });
  }
 
  Widget showImage() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 128,
            height: 128,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {

          return const Text(
            'No Image Selected',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }
 
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            showImage(),
            RaisedButton(
              child: Text("Select Image from Gallery"),
              onPressed: () {
                pickImageFromGallery(ImageSource.gallery);
              },
            ),
          ],
        ),
      );
  }
}